<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vote;
use App\Evenement;

class VoteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
      $vote = new Vote;
      $event = Evenement::firstWhere('hashAdmin', $id);
      $id = $event->id;
      $event->makeVisible("hashAdmin");
      $vote->event_id = $id;
      $vote->vote = $request->vote;
      $vote->nom = $request->nom;
      
      $vote->save();
      return response()->json($event);
    }
}
