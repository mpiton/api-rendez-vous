<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Evenement;
use App\Creneaux;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use PDO;

class EvenementController extends Controller
{

    /**
     * Enregistrement de l'event
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //Validation
      $validator = Validator::make($request->all(),[
         'titre' => 'bail|required|string|max:255',
         'nom' => 'bail|required|string|max:255',
         'lieu' => 'bail|required|string|max:255',
         'date.*' => 'bail|required|date_format:Y/m/d|after:today'
     ]);
      
     //Gestions erreurs
     $errors = $validator->errors();

     if ($validator->fails()) {
             return response()->json($errors, 401);
     }

      //Création d'un RDV
      $event = new Evenement;
      $event->makeVisible("hashAdmin");
      $event->nom = $request->nom;
      $event->lieu = $request->lieu;
      $event->titre = $request->titre;
   

      //Creation du hash
      $hashed = hash("sha256", uniqid());
      $hashedAdmin = hash("sha256", uniqid());
      $event->hash = $hashed;
      $event->hashAdmin = $hashedAdmin;
      $event->save();

      $event = Evenement::firstWhere('hash', $hashed);
      $id = $event->id;

   //Parcourir tableau de dates
   foreach ($request->date as $newDate) {
      $creneaux = new Creneaux;
      $creneaux->date = $newDate;
      $creneaux->event_id = $id;
      $creneaux->save();
   }

      // Si RDV enregistré
      return response()->json($event);

      
    }

    /**
     * Affiche l'event par son ID
     *
     * @param  \App\Evenement $event
     * @return \Illuminate\Http\Response
     */
    public function show($hash){
       $evenement = Evenement::firstWhere('hashId', $hash);
       $id =  $evenement->id;
       $event = Evenement::findOrFail($id);
       $event->makeHidden(['hashAdmin']);
       foreach ($event->creneaux as $key => $value){
          $event->creneaux[$key] = $value->date;
         }
      $event->vote;
      
      return response()->json($event, 200);
   }

    /**
     * Update l'Event
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evenement $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $event = Evenement::firstWhere('hashAdmin', $id);
      $id = $event->id;
      $event = Evenement::findOrFail($id);
      $event->update($request->all());

      return $event;
    }

    /**
     * Supprime l'Event
     *
     * @param  \App\Evenement $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evenement $event, $id)
    {
      $event = Evenement::firstWhere('hashAdmin', $id);
      $id = $event->id;
      $event = Evenement::findOrFail($id);
      $event->delete();

      return 204;
  }

      /**
     * Affiche le Panel Admin
     *
     * @param  \App\Evenement $event
     * @return \Illuminate\Http\Response
     */
    public function showAdmin($hash)
    {
      $evenement = Evenement::firstWhere('hashAdmin', $hash);

      $id =  $evenement->id;

      $event = Evenement::findOrFail($id);
      $event->makeHidden(['hashAdmin']);
      foreach ($event->creneaux as $key => $value)
      {
          $event->creneaux[$key] = $value->date;
      }

      $event->vote;
      return response()->json($event, 200);
    }

        /**
     * Ajout d'une date via le panel Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addDate(Request $request, $hash)
    {
          $validator = Validator::make($request->all(),[
              'date.*' => 'bail|required|date_format:Y/m/d|after:today'
          ]);
  
  
          $errors = $validator->errors();
  
          if ($validator->fails()) {
                  return response()->json($errors, 401);
          }
  
          $evenement = Evenement::firstWhere('hashAdmin', $hash);
          $id =  $evenement->id;
         
          foreach ($request->date as $date) {
          $creneaux = new Creneaux;
          $creneaux->date = $date;
          $creneaux->event_id = $id;
          $creneaux->save();
          }
  
          return response()->json($creneaux, 201);
         
}
};
