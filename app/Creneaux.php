<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creneaux extends Model
{
    protected $table = "creneaux";
    protected $hidden = ["created_at", "id", "updated_at", "event_id"];
}
