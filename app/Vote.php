<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
   protected $hidden = ["created_at", "id", "updated_at", "event_id"];
}
