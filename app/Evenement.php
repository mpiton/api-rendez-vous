<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
   protected $fillable = [
      'titre',
      'nom',
      'lieu'
  ];
  protected $hidden = ["created_at", "id", "updated_at", "hashAdmin"];
  public function creneaux()
  {
      return $this->hasMany(Creneaux::class, "event_id");
  }

  public function vote()
  {
      return $this->hasMany(Vote::class, "event_id");
  }
}
