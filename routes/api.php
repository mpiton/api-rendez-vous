<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//EVENTS
Route::get('evenement/{id}', 'EvenementController@show');
Route::post('evenement', 'EvenementController@store');

//Admin
Route::get('evenement/{id}/admin', 'EvenementController@showAdmin');
Route::put('evenement/{id}', "EvenementController@update");
Route::delete('evenement/{id}', "EvenementController@destroy");
Route::post('evenement/{hash}/date', 'EvenementController@addDate'); //ajout d'une date de rdv

//VOTES
Route::post('{id}/vote', 'VoteController@store');